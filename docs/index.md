# Python 연산자: 산술, 비교, 논리  <sup>[1](#footnote_1)</sup>

## 목차

1. [소개](./python-operators.md#소개)
1. [산술 연산자](./python-operators.md#산술-연산자)
1. [비교 연산자](./python-operators.md#비교-연산자)
1. [논리 연산자](./python-operators.md#논리-연산자)
1. [연산자 우선 순위와 연관성](./python-operators.md#연산자-우선-순위와-결합)
1. [마치며](./python-operators.md#마치며)


<a name="footnote_1">1</a>: [Python Tutorial 5 — Python Operators: Arithmetic, Comparison, Logical](https://python.plainenglish.io/python-tutorial-5-python-operators-arithmetic-comparison-logical-f2587a467d94)를 편역한 것이다.
